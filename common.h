#ifndef CONFIG_H
#define CONFIG_H

#include <mosquitto.h>
#include "log.h"

#define MQTT_CLIENT_ID       ""
#define MQTT_CLIENT_HOST     ""
#define MQTT_CLIENT_PORT     8883
#define MQTT_CERT_PATH       ""
#define MQTT_CLIENT_USERNAME ""
#define MQTT_CLIENT_PASSWORD ""

static inline void mosquitto_connect_handler(struct mosquitto * mosq, void * obj, int rc)
{
    (void) mosq;
    (void) rc;
    (void) obj;
    log_debug("Mosquitto connect callback called: %d", rc);
}

static inline void mosquitto_disconnect_handler(struct mosquitto * mosq, void * obj, int rc)
{
    (void) mosq;
    (void) rc;
    (void) obj;
    log_debug("Mosquitto disconnect callback called: %d", rc);
}

static inline void mosquitto_log_handler(
    struct mosquitto * mosq,
    void * obj,
    int level,
    const char * msg
)
{
    (void) mosq;
    (void) obj;

    switch (level)
    {
        case MOSQ_LOG_INFO:
        case MOSQ_LOG_NOTICE:
            log_info(msg);
            break;
            break;
        case MOSQ_LOG_WARNING:
            log_warn(msg);
            break;
        case MOSQ_LOG_ERR:
            log_error(msg);
            break;
        case MOSQ_LOG_DEBUG:
        default:
            log_debug(msg);
            break;
    }
}

#endif // CONFIG_H
