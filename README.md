# Mosquitto via Libuv

Some examples of using [Mosquitto's][mosq] (MQTT) client API within
[Libuv's][libuv] event loop. This code is for debugging purposes.

There are three programs:

- `main_threaded_loop`: uses Mosquitto's standalone loop API (no libuv at all)
- `main_poll_readwrite`: uses Mosquitto's lower level socket + read + write
  interface within libuv's API. Currently doesn't work.
- `main_poll_loop`: uses Mosquitto's all-in-one loop method within libuv. Works
  but possibly not correct. 

## Configuring

I have **not** set up any kind of server for these examples. You will need to do
that yourself. Then fill in the information in `common.h`. To observe the
behaviour I do, you'll need an MQTT server that is configured to:

- use TLS
- ...with a self-signed certificate
- ...and require a username and password

## Building

These examples use [Meson][meson] to build. I found it easiest to install Meson
in a Python 3 virtual environment using `pip`. Then just do:

```text
meson build
meson compile -C build
```

Note that `build` is the directory for build output, not a meson command. The
programs appear in `build` so you can run them with eg.

    build/mosquitto_poll_readwrite

## Resources:

- [Libuv API docs][libuv]
- [Mosquitto docs][mosq]
- [Meson][meson]

[libuv]: http://docs.libuv.org/en/v1.x/api.html
[mosq]: https://mosquitto.org/api/files/mosquitto-h.html
[meson]: https://mesonbuild.com/
