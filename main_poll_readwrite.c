#include <assert.h>
#include <mosquitto.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <uv.h>

#include "common.h"
#include "log.h"

typedef struct
{
    struct mosquitto * mosq;
    int socket;
} mosquitto_with_socket_t;

typedef struct
{
    uv_poll_t          poll_handler;
    uv_timer_t         timer_handler;
    struct mosquitto * mosq;
} mosquitto_handlers_t;

static bool mqtt_init(mosquitto_with_socket_t * ms)
{
    (void) mosquitto_lib_init();

    struct mosquitto * mosq = mosquitto_new(MQTT_CLIENT_ID, true, NULL);

    if (!mosq)
    {
        log_error("Error creating mosquitto instance: %d", errno);
        return false;
    }

    mosquitto_connect_callback_set(mosq, mosquitto_connect_handler);
    mosquitto_disconnect_callback_set(mosq, mosquitto_disconnect_handler);

    mosquitto_log_callback_set(mosq, mosquitto_log_handler);

    const int tls_result = mosquitto_tls_set(
        mosq,
        MQTT_CERT_PATH,
        NULL, NULL, NULL, NULL
    );

    if (tls_result != MOSQ_ERR_SUCCESS)
    {
        log_error("Error initialising mosquitto TLS: %d", tls_result);
        // Post error event.
        mosquitto_destroy(mosq);
        return false;
    }

    (void) mosquitto_tls_insecure_set(mosq, true);

    const int auth_result = mosquitto_username_pw_set(
        mosq,
        MQTT_CLIENT_USERNAME,
        MQTT_CLIENT_PASSWORD
    );

    assert(auth_result == MOSQ_ERR_SUCCESS);

    const int connected = mosquitto_connect(
        mosq,
        MQTT_CLIENT_HOST,
        MQTT_CLIENT_PORT,
        10
    );

    assert(connected != MOSQ_ERR_INVAL);

    if (connected != MOSQ_ERR_SUCCESS)
    {
        char error_buf[200] = {0};
        strerror_r(errno, error_buf, 200);
        log_error("Error connecting mosquitto client: %s", error_buf);
        // Post error event.
        mosquitto_destroy(mosq);
        return false;
    }

    const int mqtt_socket = mosquitto_socket(mosq);

    if (mqtt_socket == -1)
    {
        log_error("Error getting mosquitto socket");
        mosquitto_destroy(mosq);
        return false;
    }

    ms->mosq = mosq;
    ms->socket = mqtt_socket;

    return true;
}

static void mqtt_poll_handler(uv_poll_t * hdl, int status, int events)
{
    if (status < 0)
    {
        log_error("Error handling MQTT socket: %d", status);
        uv_poll_stop(hdl);
        return;
    }

    log_trace("Poll on mosquitto socket");

    mosquitto_handlers_t * mh = hdl->data;

    struct mosquitto * mosq = mh->mosq;

    const int loop_result = mosquitto_loop_misc(mosq);

    if (loop_result != MOSQ_ERR_SUCCESS)
    {
        log_error("Error servicing mosquitto loop: %d", loop_result);
    }

    if (events & UV_READABLE)
    {
        log_trace("Handling read on mosquitto socket");
        const int read_result = mosquitto_loop_read(mosq, 1);

        if (read_result != MOSQ_ERR_SUCCESS)
        {
            log_error("Error reading from mosquitto socket: %d", read_result);
        }
    }

    if ((events & UV_WRITABLE) && mosquitto_want_write(mosq))
    {
        log_trace("Handling write on mosquitto socket");
        const int write_result = mosquitto_loop_write(mosq, 1);

        if (write_result != MOSQ_ERR_SUCCESS)
        {
            log_error("Error writing to mosquitto socket: %d", write_result);
        }
    }

    int new_events = UV_READABLE;

    if (mosquitto_want_write(mosq))
    {
        log_trace("P: Waiting for write");
        new_events |= UV_WRITABLE;
    }
    else
    {
        log_trace("P: No write needed");
    }

    uv_poll_start(hdl, new_events, mqtt_poll_handler);
}

static void mqtt_timer_handler(uv_timer_t * hdl)
{
    mosquitto_handlers_t * mh = hdl->data;
    struct mosquitto * mosq = mh->mosq;

    log_trace("Handling timer");
    const int loop_result = mosquitto_loop_misc(mosq);

    if (loop_result != MOSQ_ERR_SUCCESS)
    {
        log_error("Error servicing mosquitto loop");
    }

    int new_events = UV_READABLE;

    if (mosquitto_want_write(mosq))
    {
        log_trace("T: Waiting for write");
        new_events |= UV_WRITABLE;
    }
    else
    {
        log_trace("T: No write needed");
    }

    uv_poll_start(&mh->poll_handler, new_events, mqtt_poll_handler);
}

int main(void)
{
    log_info("Started MQTT bridge");

    uv_loop_t loop;
    uv_loop_init(&loop);

    mosquitto_with_socket_t ms;

    if (!mqtt_init(&ms))
    {
        log_error("Exiting early due to MQTT failure");
        return 1;
    }

    mosquitto_handlers_t mh;

    mh.mosq = ms.mosq;

    uv_poll_init_socket(&loop, &mh.poll_handler, ms.socket);
    mh.poll_handler.data = &mh;
    uv_poll_start(&mh.poll_handler, UV_READABLE | UV_WRITABLE, mqtt_poll_handler);

    uv_timer_init(&loop, &mh.timer_handler);
    mh.timer_handler.data = &mh;
    uv_timer_start(&mh.timer_handler, mqtt_timer_handler, 0, 2000);

    log_info("Running libuv loop");
    uv_run(&loop, UV_RUN_DEFAULT);

    // No cleanup.

    return 0;
}
